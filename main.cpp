#include <iostream>
#include "Array.cpp"

using namespace std;

int main() {
    Array<int> arrayOne;
    Array<char> arrayTwo(3);
    Array<string> arrayThree(2);

    arrayOne.pushBack(1);
    arrayOne.pushBack(2);
    arrayOne[0] = 3;

    arrayTwo.pushBack('q');
    arrayTwo[1] = 'w';
    arrayTwo[2] = 'e';

    cout << "Enter 2 string: ";
    cin >> arrayThree;

    cout << "Array int: " << arrayOne <<endl;
    cout << "Array char: " << arrayTwo <<endl;
    cout << "Array string: " << arrayThree <<endl;

    Array<double> arrayComparison1;
    Array<double> arrayComparison2(4);

    cout << "Comparison\n";
    arrayComparison1.pushBack(1);
    arrayComparison1.pushBack(2);
    arrayComparison1.pushBack(3);
    arrayComparison1.pushBack(4);
    cout << "Array: ";
    cin >> arrayComparison2;

    cout << "result: ";
    if (arrayComparison1 == arrayComparison2)
        cout << "result: array one = array two";
    else if (arrayComparison1 != arrayComparison2)
        cout << "result: array one != array two";
    else if (arrayComparison1 > arrayComparison2)
        cout << "result: array one > array two";
    else if (arrayComparison1 < arrayComparison2)
        cout << "result: array one < array two";
    return 0;
}