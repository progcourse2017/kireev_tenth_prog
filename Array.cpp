//#include "Array.h"
#include <iostream>

using namespace std;

template <typename T>
class Array {
private:
    size_t size;
    unsigned top;
    T *array;

public:
    Array();
    Array(size_t s);
    Array (const Array &object);
    ~Array();

    //inline void setValue(int i, T value) { this->array[i] = value; };
    inline unsigned getEnd() { return top; };
    inline T getElementOfArray (unsigned count) { return array[count]; };

    void pushBack(const T symbol);
    void toString(ostream& os) const;

    T& operator[](int i);
    Array<T> & operator=(const T value);
    //template <class C> friend Array<C> & operator=(const Array<C> &object);
    template <class C> friend istream& operator>>(istream& is, Array<C> & object);
    template <class C> friend ostream& operator<<(ostream& os, const Array<C> &object);
    template <class C> friend bool operator==(Array<C> &object1, Array<C> &object2);
    template <class C> friend bool operator!=(Array<C> &object1, Array<C> &object2);
    template <class C> friend bool operator>=(const Array<C> &object1, const Array<C> &object2);
    template <class C> friend bool operator<=(const Array<C> &object1, const Array<C> &object2);
    template <class C> friend bool operator>(const Array<C> &object1, const Array<C> &object2);
    template <class C> friend bool operator<(const Array<C> &object1, const Array<C> &object2);
};


/*************************************************************************************************/


template <typename T>
Array<T>::Array() {
    this->size = 1;
    this->top = 0;
    array = new T [size];
}

template <typename T>
Array<T>::Array(size_t s) {
    this->size = s;
    this->top = 0;
    array = new T [size];
}

template <typename T>
Array<T>::Array(const Array &object) {
    this->size = object.size;
    this->top = object.top;
    for (unsigned count = 0; count < object.top; ++count)
        this->array[count] = object.array[count];
}

template <typename T>
Array<T>::~Array() {
    delete array;
}



template <typename T>
void Array<T>::pushBack(const T symbol) {
    if (top == size) {
        T temp[size];
        for (unsigned count = 0; count < size; ++count)
            temp[count] = array[count];
        delete array;
        size++;
        array = new T [size];
        for (unsigned count = 0; count < size; ++count)
            array[count] = temp[count];
        array[top] = symbol;
        top++;
    } else {
        array[top] = symbol;
        top++;
    }
}

template <typename T>
void Array<T>::toString(ostream &os) const {
    for (unsigned count = 0; count < top; ++count)
        os << array[count];
}



template <typename T>
T& Array<T>::operator[](int i) {
    if (i < 0 || i > this->size)
        exit(2);
    return array[i];
}

template <typename T>
Array<T> & Array<T>::operator=(const T value) {
    for (unsigned count = 0; count < this->top; ++count)
        array[count] = value;
    return *this;
}

/*template <class C>
void operator=(const Array<C> &object) {
    for (unsigned count = 0; count < object.top; ++count)
        this->array[count] = object.array[count];
    return;
}*/

template <class C>
istream& operator>>(istream &is, Array<C> &object) {
    for (unsigned count = 0; count < object.size; ++count)
        is >> object.array[count];
    object.top = object.size;
    return is;
}

template  <class C>
ostream& operator<<(ostream &os, const Array<C> &object) {
    object.toString(os);
    return os;
}

template  <class C>
bool operator==(Array<C> &object1, Array<C> &object2) {
    if (object1.getEnd() != object2.getEnd())
        return false;

    for (unsigned count = 0; count < object1.getEnd(); ++count) {
        if (object1.getElementOfArray(count) != object2.getElementOfArray(count))
            return false;
    }
    return true;
}

template  <class C>
bool operator!=(Array<C> &object1, Array<C> &object2) {
    if (object1.getEnd() != object2.getEnd())
        return true;

    for (unsigned count = 0; count < object1.getEnd(); ++count) {
        if (object1.getElementOfArray(count) != object2.getElementOfArray(count))
            return true;
    }
    return false;
}

template <class C>
bool operator>= (Array<C> &object1, Array<C> &object2) {
    if (object1.getEnd() >= object2.getEnd())
        return true;
    return false;
}

template <class C>
bool operator<= (Array<C> &object1, Array<C> &object2) {
    if (object1.getEnd() <= object2.getEnd())
        return true;
    return false;
}

template <class C>
bool operator> (Array<C> &object1, Array<C> &object2) {
    if (object1.getEnd() > object2.getEnd())
        return true;
    return false;
}

template <class C>
bool operator< (Array<C> &object1, Array<C> &object2) {
    if (object1.getEnd() < object2.getEnd())
        return true;
    return false;
}